/*
 * Power Save sample programs for DocodeModem mini
 * Copyright (c) 2020 Circuit Desgin,Inc
 * This software is released under the MIT License.
 * http://opensource.org/licenses/mit-license.php
 */

#include <docodemo.h>

DOCODEMO Dm = DOCODEMO();

volatile bool alarmTriggered = false;

void ledctrl()
{
  Dm.LedCtrl(RED_LED, ON);
  Dm.LedCtrl(GREEN_LED, ON);
  delay(1000);
  Dm.LedCtrl(RED_LED, OFF);
  Dm.LedCtrl(GREEN_LED, OFF);
}

void wakeup(void)
{
  alarmTriggered = true;
}

TaskHandle_t Handle_main_task;
static void main_task(void *pvParameters)
{
  Dm.begin();
  Dm.stopRtcTimer();

  Dm.ModemPowerCtrl(OFF);
#ifdef MINI_V2  
  Dm.exUartPowerCtrl(OFF);
  Dm.exI2CPowerCtrl(OFF);
#else
  Dm.exComPowerCtrl(OFF);
#endif
  Dm.exPowerCtrl(OFF);
  Dm.BeepVolumeCtrl(0);

  ledctrl();

  //Interrupt every 10 sec.
  Dm.setRtcTimer(RtcTimerPeriod::SECONDS, 10, wakeup);
  
  uint8_t count = 0;
  while (1)
  {
    if (count < 10)
    {
      count++;
      Dm.sleep(SleepMode::DEEPSLEEP);
      ledctrl();
    }
    else
    {
      Dm.stopRtcTimer();

      ledctrl();
      vTaskDelay(1000);
    }
  }
  
  vTaskDelete(NULL);
}

void setup()
{
  SerialDebug.begin(115200);

  vSetErrorSerial(&SerialDebug);

  xTaskCreate(main_task, "main_task", 1024, NULL, tskIDLE_PRIORITY + 1, &Handle_main_task);

  vTaskStartScheduler();

  // error: scheduler failed to start
  // should never get here
  while (1)
  {
    //SerialDebug.println("Scheduler Failed! \n");
    delay(1000);
  }
}

void loop()
{
}