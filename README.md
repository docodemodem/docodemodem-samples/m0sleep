# M0Sleep
サーキットデザイン製プログラマブル無線ユニット「どこでもでむmini」を使った省エネサンプルプログラムです。

## インストール方法

VSCodeで、File-> Add Folder to Workspaceでフォルダを追加します。

自動で必要なライブラリをgitlabからダウンロードしますが、少々時間がかかります。

そのため直ぐにコンパイルしようとするとエラーになります。

VSCodeターミナルを開いて pio lib installを実行し、エラーがなくなったらダウンロード完了です。

.pioフォルダのlibdepsフォルダを見て、Docodemo Common,DocodeModem Library,FreeRTOS_SAMD21フォルダが作成されるとダウンロードが完了したことを示します。

その後、左バーのPlatformioアイコンをクリックし、Project Tasks -> Miscellaneous -> Rebuild IntelliSense Indexをクリックして、ファイルのパスを再構築します。

.vscodeフォルダにc_cpp_properties.jsonとlaunch.jsonが作成された後、コンパイル可能になります。


**ご注意**

USBケーブルをさしているとDeepSleepに入りません。デバッグや書き込みにJTAG-ICEを使うようにしてください。
